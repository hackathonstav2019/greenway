<?php
/**
 * Created by PhpStorm.
 * User: kgs19
 * Date: 29.06.2019
 * Time: 17:06
 */

session_start();

if($_SESSION['login'] != "login"){
    header("Location: controllers/Authorization.php");
    exit;
}

if(isset($_GET['do']) && $_GET['do'] == 'logout'){
    unset($_SESSION['admin']);
    session_destroy();
    header("Location: controllers/Authorization.php");
    exit;
}

include 'header.html';
include 'map.html';
include 'footer.html';


