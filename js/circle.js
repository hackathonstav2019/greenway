/**
 * Created by kgs19 on 29.06.2019.
 */

function createCircle(lat, lon, rad) {
    // Создаем круг.
    var myCircle = new ymaps.Circle([
        // Координаты центра круга.
        [lat, lon],
        // Радиус круга в метрах.
        rad
    ], {}, {
        // Задаем опции круга.
        // Цвет заливки.
        fillColor: "#DB709377",
        // Цвет обводки.
        strokeColor: "#990066",
        // Прозрачность обводки.
        strokeOpacity: 0.8,
        // Ширина обводки в пикселях.
        strokeWidth: 5
    });

    return myCircle;
}