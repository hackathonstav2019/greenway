/**
 * Created by kgs19 on 30.06.2019.
 */

var data;

function getUserData (fileName) {
    var request = new XMLHttpRequest();
    request.open('GET', fileName);
    request.onloadend = function() {
        parse(request.responseText);
    }
    request.send();
}

getUserData('user/data.json');

function parse(obj) {
    data = JSON.parse(obj);
    $('#customer-name').append(data['name']);
    $('#car-model').append(data['carmodel']);
    $('#car-mileage').append('<strong>' + data['carmileage'] + '</strong> км');
}