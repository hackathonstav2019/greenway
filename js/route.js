/**
 * Created by kgs19 on 29.06.2019.
 */

function createRoute(myMap, addresses) {

    $('#current-position').html(addresses[0]);
    ymaps.route(addresses, {
        mapStateAutoApply: true
    }).then(function (route) {
        // Зададим содержание иконок начальной и конечной точкам маршрута.
        // С помощью метода getWayPoints() получаем массив точек маршрута.
        var points = route.getWayPoints(),
            lastPoint = points.getLength() - 1;
        // Задаем стиль метки - иконки будут красного цвета, и
        // их изображения будут растягиваться под контент.
        points.options.set('preset', 'twirl#redStretchyIcon');
        // Задаем контент меток в начальной и конечной точках.
        points.get(0).properties.set('iconContent', 'Я');
        points.get(lastPoint).properties.set('iconContent', '');

        for (var i = 0; i < points.getLength(); i++ ){
            var coordinates =  points.get(i).geometry.getCoordinates();
            myMap.geoObjects.add(createCircle(coordinates[0], coordinates[1], $('#rad').val()));
            $.ajax({
                type: 'GET',
                url: "curl.php",
                data: "lat=" + coordinates[1] + "&lon=" + coordinates[0] + "&rad=" + $('#rad').val(),
                success: function(result){
                    console.log(result);
                    var points = JSON.parse(result);
                    var lat = 0, lon = 0;
                    for (var i = 0; i < points.length; i++) {
                        lat = points[i].lat;
                        lon = points[i].lon;
                        type = points[i].type;
                        myMap.geoObjects.add(createPlacemarket(lon, lat, type));
                    }
                }
            });
        }

        route.getPaths().options.set({strokeColor: '#209F20'});

        myMap.geoObjects.add(route);

        var distance = Math.round(route.getLength() / 1000),
            message = '<span>Расстояние: ' + distance + 'км.</span><br/>';
        var moveList = '<br /><br /><b>Расстояние: '  + distance + 'км.</b><br /><br />';
        ;
        // Выводим маршрутный лист.
        $('#distance').html('');
        $('#distance').append(moveList);

    }, function (error) {
        alert('Возникла ошибка: ' + error.message);
    });
}
