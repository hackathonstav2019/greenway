/**
 * Created by kgs19 on 29.06.2019.
 */

function createPlacemarket(lat, lon, type) {
    switch (type) {
        case 1 :    icon = 'icons/bed.png';
                    hint = 'Гостиница';
                    balloon = 'Здесь можно зарядить ваш электроавтомобиль и отдохнуть';
            break;
        default :   icon = 'icons/station.png';
                    hint = 'Станция зарядки';
                    balloon = 'Здесь можно зарядить ваш электроавтомобиль';
    }

    // Создаём макет содержимого.
    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
    ),
        myPlacemarkWithContent = new ymaps.Placemark([lat, lon], {
            hintContent: hint,
            balloonContent: 'Здесь можно зарядить ваш электроавтомобиль',
            iconContent: ''
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#imageWithContent',
            // Своё изображение иконки метки.
            iconImageHref: icon,
            // Размеры метки.
            iconImageSize: [48, 48],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-24, -24],
            // Смещение слоя с содержимым относительно слоя с картинкой.
            iconContentOffset: [15, 15],
            // Макет содержимого.
            iconContentLayout: MyIconContentLayout
        });

        return myPlacemarkWithContent;
}
