/**
 * Created by kgs19 on 29.06.2019.
 */

/**
 * Create the Google Map
 */

ymaps.ready();

function init() {
    $('#map').html('');
    $('#list').html('');

    var myMap = new ymaps.Map("map", {
        center: [$('#lat').val(),$('#lon').val()],
        zoom: 5,
        type: 'yandex#map',
        behaviors: ['scrollZoom', 'drag'],
        controls: ['typeSelector', 'zoomControl']
    });

    myMap.controls.add('zoomControl', {size: "small", top: "40px", right: "10px"});
    myMap.controls.add('typeSelector');

    // строим маршрут
    var addresses = [];
    addresses.push($('#address_from').val());
    addresses.push($('#address_to').val());

    ymaps.route(addresses, {
        mapStateAutoApply: true
    }).then(function (route) {
        var points = route.getWayPoints(),
            lastPoint = points.getLength() - 1;

        startPoint = points.get(0).geometry.getCoordinates();
        endPoint = points.get(lastPoint).geometry.getCoordinates();

        $.ajax({
            type: 'GET',
            url: "controllers/Routes.php",
            data: "latBeg=" + startPoint[1] + "&lonBeg=" + startPoint[0] +
            "&latEnd=" + endPoint[1] + "&lonEnd=" + endPoint[0] + "&userId=81&charged=100",
            success: function(result){
                var points = JSON.parse(result);
                var newPoints = [];
                for (var i = 0; i < points.length; i++) {
                    lat = points[i].lat;
                    lon = points[i].lon;

                    newPoints.push(lon + ', ' + lat);
                }

                createRoute(myMap, newPoints);
            }
        });
    });

    /*
    if (cities) {
        var citiesList = $('.cities');
        citiesList.html('')
        for (var i = 0; i < cities.length; i++) {
            citiesList.append('<p><input class="input-bottom-border" value="'+ cities[i] +'"></p>');
        }
    };
    */
}

