<?php
/**
 * Created by PhpStorm.
 * User: kgs19
 * Date: 29.06.2019
 * Time: 14:33
 */

// локальный сервер
// $pdo = new PDO("pgsql:dbname=postgis_25_sample;host=localhost", 'postgres', 'qw12QW12' );

// удаленный сервер
$pdo = new PDO("pgsql:dbname=postgis_25_sample;host=10.206.116.124", 'postgres', '15061993' );

// тест подключения к postgres
if ($pdo) {
    // $result = $pdo->query('SELECT item_value FROM test');
    $result = $pdo->query('SELECT * FROM public."Hostels"');
    while ($row = $result->fetch())
    {
        echo $row['ID'] . "\n";
    }
} else {
    die('Не удалось соединиться: ' . pg_last_error());
}

