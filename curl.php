<?php
/**
 * Created by PhpStorm.
 * User: kgs19
 * Date: 29.06.2019
 * Time: 17:44
 */

$ch = curl_init();

// установка URL и других необходимых параметров
$str = "http://10.206.116.124:5000/greenroad/api/v1/neighbor?";
$str .= 'lat=' . $_REQUEST['lat'];
$str .= "&lon=" . $_REQUEST['lon'];
$str .= "&rad=" . $_REQUEST['rad'];


curl_setopt($ch, CURLOPT_URL, $str);
curl_setopt($ch, CURLOPT_HEADER, 0);

// загрузка страницы и выдача её браузеру
curl_exec($ch);

// завершение сеанса и освобождение ресурсов
curl_close($ch);